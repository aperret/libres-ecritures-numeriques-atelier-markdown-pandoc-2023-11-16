---
title: Pandoc Markdown
author: Arthur Perret
date: 16 novembre 2023
lang: fr
---

# Introduction

Pandoc est un programme de conversion entre formats de balisage. Parmi les formats reconnus par Pandoc, il y a une variante de Markdown qui lui est propre, et qui ajoute des fonctionnalités liées à l'écriture scientifique.

# Quelques exemples (liste non exhaustive)

## Tableaux

| Right | Left | Default | Center |
|------:|:-----|---------|:------:|
|   12  |  12  |    12   |    12  |
|  123  |  123 |   123   |   123  |
|    1  |    1 |     1   |     1  |

: Exemple d'une des syntaxes pour écrire des tableaux en Pandoc Markdown

## Références bibliographiques

L'écriture joue un rôle dans l'organisation des sociétés [@goody1986].

## Listes de définitions

Langage de balisage
: En informatique, les langages de balisage représentent une classe de langages spécialisés dans l'enrichissement d'information textuelle. (Wikipédia)

Langage de balisage léger
: Un langage de balisage léger est un type de langage de balisage utilisant une syntaxe simple, conçu pour être facile à saisir avec un simple éditeur de texte, et facile à lire dans sa forme non formatée. (Wikipédia)

## Formules mathématiques

$a^2 + b^2 = c^2$

## Notes de bas de page

Du texte^[Une note.]. Encore du texte[^ndbp].

# Le Pandoc Markdown comme matrice textuelle unique

Il est tout à fait possible d'utiliser Markdown comme « format d'entrée » universel pour Pandoc : ceci permet d'écrire dans un format texte simple, léger et pérenne, et de générer à la volée des documents dans tous les formats que peut fabriquer Pandoc.

# Références {-}

[^ndbp]: Voici une note de bas de page. L'identifiant entre crochets n'a aucune signification, il suffit qu'il soit unique dans le document.