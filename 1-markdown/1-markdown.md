# Markdown

[Markdown](https://fr.wikipedia.org/wiki/Markdown) est un [langage de balisage léger](https://fr.wikipedia.org/wiki/Langage_de_balisage_léger) inventé par [John Gruber](https://fr.wikipedia.org/wiki/John_Gruber) avec l'aide d'[Aaron Swartz](https://fr.wikipedia.org/wiki/Aaron_Swartz) en 2004. C'est à la fois une syntaxe pour structurer des contenus au [format texte](https://www.arthurperret.fr/cours/format-texte.html), et un programme de conversion vers HTML de ces contenus structurés.

La syntaxe de Markdown s'inspire des nombreuses formes d'écriture utilisées durant les débuts du Web, notamment dans les forums et les canaux de messagerie directe. Ces écritures utilisaient les caractères du clavier (comme `-`, `>`, `*`) pour symboliser différentes choses comme des listes, des citations ou de l'emphase.

> “The overriding design goal for Markdown’s formatting syntax is to make it **as readable as possible**.” John Gruber <https://daringfireball.net/projects/markdown/>