# Atelier Markdown et Pandoc

Proposé par Arthur Perret dans le cadre de la journée Libres écritures numériques du 16 novembre 2023

<http://barthes.enssib.fr/atelier/Ecritures2023.html>

## Markdown

Informations générales :

- La page du projet sur le site de son créateur John Gruber : <https://daringfireball.net/projects/markdown/>
- Page Wikipédia : <https://fr.wikipedia.org/wiki/Markdown>
- Sur mon site : <https://www.arthurperret.fr/cours/markdown.html>

Tutoriel : <https://www.arthurperret.fr/tutomd/>

## Pandoc

Informations générales :

- Site officiel : <https://pandoc.org/>
- Page Wikipédia : <https://fr.wikipedia.org/wiki/Pandoc>
- Sur mon site : <https://www.arthurperret.fr/cours/pandoc.html>

## Pandoc Markdown

<https://pandoc.org/MANUAL.html#pandocs-markdown>

Pandoc Markdown ajoute à Markdown des formes d'écriture utilisées en sciences.

Extensibilité :

- balisage sémantique (div/span natifs)
- programmation éditoriale (Lua)

## Make

<https://www.gnu.org/software/make/manual/make.html>

Outil d'automatisation, de recettage.

Tutoriel : <https://makefiletutorial.com/>

## Make + Pandoc

Pour automatiser le déclenchement de plusieurs commandes Pandoc pour fabriquer plusieurs choses à partir des mêmes fichiers.

Pour tester la démo dans ce dépôt, se déplacer dans le répertoire "3-pandoc-make" et exécuter la commande `make`.

Exemple de modèle : <https://www.arthurperret.fr/blog/2022-06-22-publication-multiformats-pandoc-make.html>

## Quarto

<https://quarto.org/>

Pour faire du Pandoc comme avec Make mais avec un système éditorial clé en main. Inclut beaucoup de choses, ce qui fait gagner du temps : modèles d'export (ex: modèle complet de site web), fonctionnalités basées sur des plugins de l'écosystème Pandoc (ex: références croisées avec pandoc-crossref).

Exemple de modèle pour une thèse multiformats :

- Dépôt : <https://gitlab.huma-num.fr/aperret/quarto-phd-template/>
- Billet : <https://www.arthurperret.fr/blog/2023-09-14-ecriture-relecture-publication-these-multi-formats-quarto.html>

## Cosma

<https://cosma.arthurperret.fr>

Application permettant de générer une documentation hypertextuelle autoportante, partageable. Sur base de Pandoc Markdown. Agrège beaucoup de choses bien connues et qui existent ailleurs. Une grosse nouveauté, qui arrive bientôt : l'intégration des données bibliographiques au graphe.

Guide de démarrage / tutoriel : <https://cosma.arthurperret.fr/getting-started.html#getting-started-with-cosma>